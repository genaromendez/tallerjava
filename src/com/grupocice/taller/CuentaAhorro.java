/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupocice.taller;

/**
 *
 * @author macpro1
 */
public class CuentaAhorro extends CuentaCorriente {
    
    private double interes;

    public CuentaAhorro(Titular titular, String cuenta, double saldo, double interes) {
        super(titular, cuenta, saldo);
        this.interes = interes;
    }
    
    public CuentaAhorro(Titular titular, String cuenta) {
        super(titular, cuenta);
        this.interes = 2.5;
    }    
    
    public void setInteres(double interes){
        this.interes = interes;
    }
    
    public void calculaInteres(){
        this.setSaldo(this.getSaldo()+(this.getSaldo()*(this.interes/100)));
     // this.setSaldo(this.getSaldo()*(1+(this.interes/100));
    }
    
}
