/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupocice.taller;

/**
 *
 * @author macpro1
 */
public class CuentaCorriente {
    private Titular titular;
    private String  numeroDeCuenta;
    private double  saldo;
    /*
    Tenga un constructor con parámetros de tipo Titular, String y double.
    Tenga un constructor con parámetros de tipo Titular y String. El saldo
    des inicializará a 15.3
    */
    
    public CuentaCorriente(){
        
    }
    
    public CuentaCorriente(Titular titular, 
                           String cuenta, double saldo){
        this.titular = titular;
        this.numeroDeCuenta = cuenta;
        this.saldo = saldo;
    }
    
    public CuentaCorriente(Titular titular, String cuenta){
        this.titular = titular;
        this.numeroDeCuenta = cuenta;
        this.saldo = 15.3;
    }
    
    public void setTitular(Titular titular){
        this.titular = titular;
    }
    
    public void setCuenta(String cuenta){
        this.numeroDeCuenta = cuenta;
    }
    
    public void setSaldo(double saldo){
        if (saldo>=0){
            this.saldo = saldo;
        } else {
            System.err.println("No se pueden asignar valores negativos al saldo");
        }
    }
    
    public void ingresar(double cantidad){
        this.saldo += cantidad;
    }

    public void reintegro(double cantidad){
        if (this.saldo>=cantidad){
            this.saldo -= cantidad;
        }      
    }    
    
    public String getCuenta(){
        return this.numeroDeCuenta;
    }
    
    public double getSaldo(){
        return this.saldo;
    }
    
    public int compareTo(CuentaCorriente cuenta){
        return this.numeroDeCuenta.compareTo(cuenta.numeroDeCuenta);
    }
    
    public boolean equals(CuentaCorriente cuenta){
        return this.numeroDeCuenta.equals(cuenta.numeroDeCuenta);
    }
    
    public void print(){
        this.titular.print("");
    }
    
}
