/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grupocice.taller;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.event.ComponentEvent;

/**
 *
 * @author macpro1
 */
public class Ejemplo1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        
        Titular genaro   = new Titular("Genaro","Mendez",46);
        Titular consuelo = new Titular("Consuelo","Tapia",18);

        Ejemplo1.metodo1(genaro.nombre);
        
        Ejemplo1 e1 = new Ejemplo1();
        e1.metodo1(consuelo.nombre);
        
        CuentaAhorro ca1 = new CuentaAhorro(genaro,"12345");
        
        CuentaAhorro ca2 = new CuentaAhorro(consuelo,"54321",100,5.0);
        
        if(ca1.compareTo(ca2)==0){
            System.out.println("Son iguales");
        } else {
            System.out.println("No Son iguales");
        }
        
        ca2.calculaInteres();
        
        System.out.println(ca2.getCuenta());
        System.out.println(ca2.getSaldo());
   
    }
    
    public static void metodo1(String cad){
        System.out.printf("%s\n",cad);
    }

}
